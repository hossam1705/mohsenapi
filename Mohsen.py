import numpy as np
import pandas as pd
import re
def padding(sentence, max_len):
  new_sent = []
  for i in range(max_len):
      try:
          clean = re.sub(r'[^ a-z A-Z 0-9]', " ", sentence[i])
          clean = clean.lower()
          new_sent.append(clean)
      except:
          new_sent.append("pad")
  return new_sent
def predictIntent(text, idx2Intent, words2index, model):
  words = padding(text.split(), 16)
  X = []
  i = 0
  for w in words:
    try:
      X.append(words2index[w])
    except:
        X.append(0)
    i = i + 1
  X = np.array(X).reshape(1, len(X))
  prediction = model.predict_classes(X)
  return idx2Intent[prediction[0]]
def predictNER(text, idx2tag, words2index, model):
  clean = re.sub(r'[^ a-z A-Z 0-9]', " ", text)
  words = padding(clean.split(), 17)
  X = []
  i = 0
  for w in words:
    try:
      X.append(words2index[w])
    except:
      if(i < len(text.split())):
        X.append(words2index["xxx"])
      else:
        X.append(words2index["pad"])
    i = i + 1
  X = np.array(X).reshape(1, len(X))
  prediction = model.predict_classes(X)
  tags = []
  for i in range(len(text.split())):
    tags.append([text.split()[i], idx2tag[prediction[0][i]]])
  return tags
def getIntent(text, model):
  wordsseq = pd.read_csv('Intent_word_sequences.csv', encoding = "latin1", names = ["Sequence", "Word"])
  Intentsseq = pd.read_csv('Intent_sequences.csv', encoding = "latin1", names = ["Sequence", "Tag"])
  Intents2index = {Intentsseq["Tag"][i]:int(Intentsseq["Sequence"][i]) for i in range(len(Intentsseq["Tag"]))}
  idx2Intent = {i: w for w, i in Intents2index.items()}
  words2index = {wordsseq["Word"][i]:int(wordsseq["Sequence"][i]) for i in range(len(wordsseq["Word"]))}
  prediction = predictIntent(text, idx2Intent, words2index, model)
  return prediction
def getNer(text, model):
  wordsseq = pd.read_csv('NER_word_sequences.csv', encoding = "latin1", names = ["Sequence", "Word"])
  tagsseq = pd.read_csv('NER_tags_sequences.csv', encoding = "latin1", names = ["Sequence", "Tag"])
  tags2index = {tagsseq["Tag"][i]:int(tagsseq["Sequence"][i]) for i in range(len(tagsseq["Tag"]))}
  idx2tag = {i: w for w, i in tags2index.items()}
  words2index = {wordsseq["Word"][i]:int(wordsseq["Sequence"][i]) for i in range(len(wordsseq["Word"]))}
  prediction = predictNER(text, idx2tag, words2index, model)
  return prediction
def testmohse():
    return "working"