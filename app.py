from flask import Flask, jsonify, request
from tensorflow.python.keras.models import load_model
import Mohsen

app = Flask(__name__)
intentModel = load_model("mohsen draft 5.h5")
entitiesModel = load_model("NER mohsen draft 1.h5")

@app.route('/', methods=['GET'])
def index():
    return jsonify(
        {   "status": "service running"
        })

@app.route('/predict/intent', methods=['GET'])
def predictI():
    text = request.args.get('text')
    return jsonify(
        {   "result": Mohsen.getIntent(text, intentModel)
        })
@app.route('/predict/ner', methods=['GET'])
def predictN():
    text = request.args.get('text')
    return jsonify(
        {   "result": Mohsen.getNer(text, entitiesModel)
        })
if __name__ == '__main__':
    app.run()